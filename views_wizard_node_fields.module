<?php


/*
 * hook_menu: the URL for this wizard
 */
function views_wizard_node_fields_menu() {
  $items['views-wizard/node-fields'] = array(
    'title' => t("Views Wizard: Add all node type's fields to View"),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('form_views_wizard_node_fields'),
    'access callback' => 'user_access',
    'access arguments' => array('administer views'),
    'description' => t('Create a view with fields from a specific node type'),
    'type' => MENU_CALLBACK, //MENU_LOCAL_TASK,
    'file' => 'views_wizard_node_fields.inc',
  );
  return $items;
} 


/*
 * The process for this wizard
 */
function views_wizard_node_fields_process($node_type, $base_view = NULL, $view_name = NULL) {
 
  // Get or create our view to add fields to

  // Create a new view with specified name from an existing view
  if($view_name && $base_view) {
    $base_view = views_get_view($base_view);
    $view = $base_view->clone_view();
    $base_view->destroy();
    $view->name = $view_name;
    $view->save();
  }
  // Create a new view with specified name
  elseif($view_name && !$base_view) {
    //create a vanilla node view
    $view = views_wizard_node_fields_new_view($view_name);
  }
  // Use an existing view
  elseif(!$view_name && $base_view) {
    $view = views_get_view($base_view);
  }
  // Create a new view with a generated name
  elseif(!$view_name && !$base_view) {
    $view_name = "views_wizard_node_fields_" . strtotime("now"); 
    // create a vanilla node view
    $view = views_wizard_node_fields_new_view($view_name);
  }
  
  // Get the fields for the requested node type
  $content_type = content_types($node_type);

  // Inefficient way to get all the fields, but may find a way to use this later
  // for ensuring that we are feeding View's add_item() correctly.
  // $content_type_info = _content_type_info();

  // Add each field to the view
  foreach($content_type['fields'] as $key => $value) {
    $view->add_item(
      'default', 
      "field", 
      //$content_type['tables']['content_'. $value['field_name']], // this reference to a table is not what Views is looking for...
      'node_data_' . $value['field_name'],
      $value['field_name'] . "_value", // _value suffix because we don't want delta or count or whatevs
      $options);  
  }

  // Save our modifications to the view
  $view->save();
}


/*
 * Create a new vanilla view with node as the base table
 */
function views_wizard_node_fields_new_view($view_name) {
  $view = views_new_view();
  $view->name = $view_name;
  $view->description = '';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; 
  $handler = $view->new_display('default', 'Defaults', 'default');
  
  // Save the vanilla view
  $view->save();
  return $view;
}
