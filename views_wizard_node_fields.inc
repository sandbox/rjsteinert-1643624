<?php

/*
 * The Form UI for this wizard
 * 
 * @todo There are four potential outcomes of this form.  It would be more user friendly
 * to break it up.
 * 
 */

function form_views_wizard_node_fields(&$form_state) {
  
  $content_types = content_types();
  /*
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('A view wizard.'),
  );
   * 
   */
  
  $views = array_keys(views_get_all_views());
  asort($views);
  array_unshift($views, "Start with a new view");
  $form['base_view'] = array(
    '#type' => 'select',
    // '#title' => t('Base view'),
    '#title' => t('View to add fields to'),
    '#required' => TRUE,
    '#options' => $views
  );
  
  $node_types = array_keys($content_types);
  asort($node_types);
  $form['node_type'] = array(
    '#type' => 'select',
    '#title' => t('Content type whose fields you would like to add'),
    '#required' => TRUE,
    '#options' => $node_types
  );
  $form['view_name'] = array(
    '#type' => 'textfield',
    '#title' => "If you are starting with a new view, enter a name for that new view here",
    //'#title' => t('Enter a name for a new View if you A) want to clone the base View you have selected above, or B) 
    //    you have selected "Start with a new view" as a base view'),
    '#required' => FALSE, 
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

function form_views_wizard_node_fields_validate($form, &$form_state) {
  if($form["view_name"]["#value"] && !($form["base_view"]["#value"] == 0 || $form["base_view"]["value"] == 1)) {
    form_set_error("View cloning not currently possible.", t("You can currently only add fields to existing views or to a new view.  If you want to add fields to a new view that is based on another view, use the Views admin to clone the base view you want and then select that clone here as your base View."));
  }
  if($form['view_name']['#value'] && $view = views_get_view($form['view_name']['#value'])) {
    form_set_error('View Exists', t('A view with that name already exists. Choose a different name.'));
  }
   
}

/**
 * Adds a submit handler/function to our form to send a successful
 * completion message to the screen.
 */

function form_views_wizard_node_fields_submit($form, &$form_state) {
  
  if($form['base_view']['#value'] == 0 || $form['base_view']['#value'] == 1) {
  // The option to create a new view has been selected, no base view.
    $base_view = NULL;
  }
  else {
    // A base view has been selected, set base view to the ID of the base view.
    $base_view = $form['base_view']['#options'][$form['base_view']['#value']];
  }
  
	views_wizard_node_fields_process(
    $form['node_type']['#options'][$form['node_type']['#value']], 
    $base_view,
    $form['view_name']['#value']
  );
  drupal_set_message(t('View: @name has been submitted.',
    array('@name' => $form_state['values']['view_name'])
  ));
}
